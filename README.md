**proto objects - src/main/proto**  
Building.proto (Proto object for building table)  
Employee.proto (Proto object for employee table)  
  
---  
  
**csv data - src/resources/**   
csv files to populate Building and Employee table  
  
---  
**Assignment 4a**  
  
## CSVtoProto.java  

Creates serialized proto file from above mentioned csv  
Output path - resources/ProtoOutputFiles  
  
**creating tables**  
  
## UploadFiletoHDFS.java  
To store serialized proto file from local to HDFS  
  
## HbaseTableCreator.java  
To create Building and Employee table  
Uses Admin.createTable method  
  
## Main.java  
Calls UploadFiletoHDFS and HbaseTableCreator methods internally  
Contains main method  
  
**insert data into hbase tables**  
  
## MRDriver.java  
Driver class for mapreduce job for creating HFiles  
Calls driver method for Employee and Building each  
Calls bulkLoad method after job completion  
  
## HbaseMapper.java  
Writes data into Put objects with column qualifiers:  
1. employee - Employee_Id,Name,Building_Code,Floor_Number,Salary,Department  
2. building - building_code, floors, companies, cafeteria_code  
  
---  
  
**Assignment 4b**  
  
## CafeteriaDriver.java  
Scans tables employee and building and apply joins based on building_code  
Mapper assigns building code to each Employee row - Iterator(Result)  
Reducer inserts cafe code through Put object into employee table  
**Mapper class - CafeteriaMapper.java  
Reducer class - CafeteriaReducer.java**  
  
---  
