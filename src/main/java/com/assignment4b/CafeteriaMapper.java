package com.assignment4b;

import ProtoFiles.Building.Building;
import ProtoFiles.Employee.Employee;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;

import static com.assignment4b.util.Constants.BUILDING_TABLE_BYTES;
import static com.assignment4b.util.Constants.EMPLOYEE_TABLE_BYTES;

public class CafeteriaMapper extends TableMapper<Text, Result> {

    public CafeteriaMapper() {
        System.out.println("Mapper Class called");
    }

    @Override
    protected void map(ImmutableBytesWritable key, Result result,
                       Mapper<ImmutableBytesWritable, Result, Text, Result>.Context context)
            throws IOException, InterruptedException {
        TableSplit tableSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = tableSplit.getTableName();

        if (Arrays.equals(tableName, EMPLOYEE_TABLE_BYTES)) {
            //Employee_Id,Name,Building_Code,Floor_Number,Salary,Department
            byte[] cf = Bytes.toBytes("employee");
            byte[] cqBuildingCode = Bytes.toBytes("building_code");
            String building_code = Bytes.toString(result.getValue(cf, cqBuildingCode));
            context.write(new Text(building_code), result);

        } else if (Arrays.equals(tableName, BUILDING_TABLE_BYTES)) {
            // building_code, total_floors, total_companies, cafeteria_code
            byte[] cf = Bytes.toBytes("building");
            byte[] cqBuildingCode = Bytes.toBytes("building_code");
            String building_code = Bytes.toString(result.getValue(cf, cqBuildingCode));
            context.write(new Text(building_code), result);
        }
    }
}