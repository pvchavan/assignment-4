package com.assignment4b;

import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import ProtoFiles.Employee.Employee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.assignment4b.util.Constants.*;

public class CafeteriaReducer extends TableReducer<Text, Result, ImmutableBytesWritable> {

    public CafeteriaReducer() {
        System.out.println("Reached Reducer Class");
    }

    Map<String, String> buildingCafe = new HashMap<>();

    @Override
    protected void reduce(Text key, Iterable<Result> values,
                          Reducer<Text, Result, ImmutableBytesWritable, Mutation>.Context context)
            throws IOException, InterruptedException {

        List<Employee> employeeList = new ArrayList();
        for (Result result : values) {
            if (result.containsColumn(Bytes.toBytes(EMPLOYEE_COLUMN_FAMILY),
                    Bytes.toBytes("name"))) {

                byte[] cf = Bytes.toBytes("employee");
                byte[] cqEmpId = Bytes.toBytes("employee_id");
                byte[] cqBuildingCode = Bytes.toBytes("building_code");

                Employee.Builder employee = Employee.newBuilder();
                employee.setEmployeeId(Integer.parseInt(Bytes.toString(result.getValue(cf, cqEmpId))))
                        .setBuildingCode(Bytes.toString(result.getValue(cf, cqBuildingCode)));
                employeeList.add(employee.build());

            }
            else if (result.containsColumn(Bytes.toBytes(BUILDING_COLUMN_FAMILY),
                    Bytes.toBytes("building_code"))) {

                byte[] cf = Bytes.toBytes("building");
                byte[] cqBuildingCode = Bytes.toBytes("building_code");
                byte[] cqCafeCode = Bytes.toBytes("cafeteria_code");
                String building_id = Bytes.toString(result.getValue(cf, cqBuildingCode));
                String cafeteriaCode = Bytes.toString(result.getValue(cf, cqCafeCode));
                if (!buildingCafe.containsKey(building_id)) {
                    buildingCafe.put(building_id, cafeteriaCode);
                }
            }
        }

        for (Employee employee : employeeList) {
            Put put = insertCafeteriaCode(employee);
            int employee_id = employee.getEmployeeId();
            context.write(null, put);
        }
    }

    public Put insertCafeteriaCode(Employee employee) {
        // employee = employee.toBuilder().setCafeteriaCode(cafe_value).build();
        String buildingCode = employee.getBuildingCode();
        String employee_id = String.valueOf(employee.getEmployeeId());
        System.out.println("employee: " + employee_id + " - "
                + buildingCode + " - " + buildingCafe.get(buildingCode));
        Put put = new Put(Bytes.toBytes(employee_id));
        put.addColumn(Bytes.toBytes("employee"), Bytes.toBytes("cafeteria_code"),
                Bytes.toBytes(buildingCafe.get(buildingCode)));
        return put;
    }
}